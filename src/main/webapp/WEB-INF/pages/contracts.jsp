<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Contracts Page</title>

    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #ccc;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
        }

        .tg .tg-4eph {
            background-color: #f9f9f9
        }
    </style>
</head>
<body>
<a href="../../index.jsp">Back to main menu</a>

<br/>
<br/>

<h1>Contract List</h1>

<c:if test="${!empty contractList}">
    <table class="tg">
        <tr>
            <th width="80">ID</th>
            <th width="120">Title</th>
            <th width="120">Author</th>
            <th width="120">Price</th>
            <th width="60">Edit</th>
            <th width="60">Delete</th>
        </tr>
        <c:forEach items="${contractList}" var="contract">
            <tr>
                <td>${contract.id}</td>
                <td><a href="/contractdata/${contract.id}" target="_blank">${contract.contractNumber}</a></td>
                <td>${contract.contractTariff}</td>
                <td>${contract.options}</td>
                <td><a href="<c:url value='/edit/${contract.id}'/>">Edit</a></td>
                <td><a href="<c:url value='/remove/${contract.id}'/>">Delete</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>


<h1>Add a Contract</h1>

<c:url var="addAction" value="/contracts/add"/>

<form:form action="${addAction}" commandName="contract">
    <table>
        <c:if test="${!empty contract.contractNumber}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="id"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <form:label path="contractTariff">
                    <spring:message text="ContractTariff"/>
                </form:label>
            </td>
            <td>
                <form:input path="contractTariff"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="options">
                    <spring:message text="Options"/>
                </form:label>
            </td>
            <td>
                <form:input path="options"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <c:if test="${!empty contract.contractNumber}">
                    <input type="submit"
                           value="<spring:message text="Edit Contract"/>"/>
                </c:if>
                <c:if test="${empty book.bookTitle}">
                    <input type="submit"
                           value="<spring:message text="Add Contract"/>"/>
                </c:if>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>
