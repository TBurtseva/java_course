package core.model;

import javax.persistence.*;
import java.awt.*;

@Entity
@Table(name = "CONTRACTS")
public class Contract {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "CONTRACT_NUMBER")
    private int contractNumber;

    private Tariff tariff;///????

    private Customer customer;//?????




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(int contractNumber) {
        this.contractNumber = contractNumber;
    }


    @Override
    public String toString() {
        return "Contract{" +
                "id=" + id +
                ", contractNumber=" + contractNumber +
                '}';
    }
}
