package core.model;

import javax.persistence.*;

@Entity
@Table (name = "OPTION")
public class Option {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PRICE")
    private double price;

    @Column(name = "ONE_TIME_PRICE")
    private double oneTimePrice;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getOneTimePrice() {
        return oneTimePrice;
    }

    public void setOneTimePrice(double oneTimePrice) {
        this.oneTimePrice = oneTimePrice;
    }

    @Override
    public String toString() {
        return "Option{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", oneTimePrice=" + oneTimePrice +
                '}';
    }
}
