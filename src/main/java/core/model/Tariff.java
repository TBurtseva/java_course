package core.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "TARIFF")
public class Tariff {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "TARIFF_NAME")
    private int contractNumber;

    @Column(name = "PRICE")
    private String contractTariff;

    @ManyToMany
    @JoinTable(name = "TARIFF_OPTION",
    joinColumns = @JoinColumn(name = "TARIFF_ID", referencedColumnName = "ID"),
    inverseJoinColumns = @JoinColumn(name = "OPTION_ID", referencedColumnName = "ID"))
    private List<Option> options;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(int contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getContractTariff() {
        return contractTariff;
    }

    public void setContractTariff(String contractTariff) {
        this.contractTariff = contractTariff;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id=" + id +
                ", contractNumber=" + contractNumber +
                ", contractTariff='" + contractTariff + '\'' +
                ", options=" + options +
                '}';
    }
}
