package core.service;


import core.dao.ContractDao;
import core.model.Contract;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class ContractServiceImpl implements ContractService {
    private ContractDao contractDao;

    public void setContractDao(ContractDao contractDao) {
        this.contractDao = contractDao;
    }

    @Override
    @Transactional
    public void addContract(Contract contract) {
        this.contractDao.addContract(contract);
    }

    @Override
    @Transactional
    public void updateContract(Contract contract) {
        this.contractDao.updateContract(contract);
    }

    @Override
    @Transactional
    public void removeContract(int id) {
        this.contractDao.removeContract(id);
    }

    @Override
    @Transactional
    public Contract getContractById(int id) {
        return this.contractDao.getContractById(id);
    }

    @Override
    @Transactional
    public List<Contract> contractList() {
        return this.contractDao.contractList();
    }
}
