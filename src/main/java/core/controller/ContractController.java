package core.controller;


import core.model.Contract;
import core.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ContractController {
    private ContractService contractService;

    @Autowired(required = true)
    @Qualifier(value = "contractService")
    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    @RequestMapping(value = "contracts", method = RequestMethod.GET)
    public String contractList(Model model){
        model.addAttribute("contract", new Contract());
        model.addAttribute("contractList", this.contractService.contractList());
        return "contracts";
    }

    @RequestMapping(value = "/contracts/add", method = RequestMethod.POST)
    public String addContract(@ModelAttribute("contract") Contract contract){
        if(contract.getId() == 0){
            this.contractService.addContract(contract);
        }else {
            this.contractService.updateContract(contract);
        }
        return "redirect:/contracts";
    }

    @RequestMapping("/remove/{id}")
    public String removeContract(@PathVariable("id") int id){
        this.contractService.removeContract(id);
        return "redirect:/books";
    }

    @RequestMapping("edit/{id}")
    public String editContract(@PathVariable("id") int id, Model model){
        model.addAttribute("contract", this.contractService.getContractById(id));
        model.addAttribute("contractList", this.contractService.contractList());
        return "contracts";
    }

    @RequestMapping("contractdata/{id}")
    public String contractData(@PathVariable("id") int id, Model model){
        model.addAttribute("contract", this.contractService.getContractById(id));
        return "contractdata";
    }
}
