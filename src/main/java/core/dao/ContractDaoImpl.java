package core.dao;

import core.model.Contract;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ContractDaoImpl implements ContractDao {

    private static final Logger logger = LoggerFactory.getLogger(ContractDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addContract(Contract contract) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(contract);
        logger.info("Book successfully saved. Book details: " + contract);
    }

    @Override
    public void updateContract(Contract contract) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(contract);
        logger.info("Book successfully update. Book details: " + contract);
    }

    @Override
    public void removeContract(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Contract contract = (Contract) session.load(Contract.class, new Integer(id));

        if(contract!=null){
            session.delete(contract);
        }
        logger.info("Book successfully removed. Book details: " + contract);
    }

    @Override
    public Contract getContractById(int id) {
        Session session =this.sessionFactory.getCurrentSession();
        Contract contract = (Contract) session.load(Contract.class, new Integer(id));
        logger.info("Book successfully loaded. Book details: " + contract);
        return contract;

    }

    @Override
    public List<Contract> contractList() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Contract> contractList = session.createQuery("from Contract").list();

        for(Contract contract: contractList){
            logger.info("Contract list: " + contract);
        }
        return contractList();
    }
}
